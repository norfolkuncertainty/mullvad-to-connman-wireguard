# Details
Converts Mullvad wireguard configs to usable configs for use on LibreElec

## Usage

Download generated config from [here](https://mullvad.net/en/account/#/wireguard-config/)

Run script on downloaded files
`./mullvad_to_connman_wireguard.sh ~/Downloads/mullvad-au4.conf`

Copy Config files to LibreElec
`scp mullvad-au4.config root@kodi:/storage/.config/wireguard/`

Follow testing instructions from [here](https://wiki.libreelec.tv/wireguard)
