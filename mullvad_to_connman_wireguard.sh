#! /usr/bin/env bash
MULVADFILE=$1
BASENAME=$(basename $MULVADFILE | awk -F. '{print $1}')
CONNMANFILE=$BASENAME.config

HOST=$(awk -F':|=' '/Endpoint/{print $2}' $MULVADFILE)
PORT=$(awk -F':|=' '/Endpoint/{print $3}' $MULVADFILE)
ADDRESS=$(awk -F'=|,' '/Address/{print $2}' $MULVADFILE)
PRIVATEKEY=$(awk '/PrivateKey/{print $3}' $MULVADFILE)
PUBLICKEY=$(awk '/PublicKey/{print $3}' $MULVADFILE)
DNS=$(awk '/DNS/{print $3}' $MULVADFILE)
ALLOWEDIPS=$(awk -F'=|,' '/Allowed/{print $2}' $MULVADFILE)

echo "
[provider_wireguard]
Type = WireGuard
Name = $BASENAME
Host = $HOST
Domain = my.home.vpn
WireGuard.Address = $ADDRESS
WireGuard.PrivateKey = $PRIVATEKEY
WireGuard.PublicKey = $PUBLICKEY
WireGuard.DNS = $DNS
WireGuard.AllowedIPs = $ALLOWEDIPS
WireGuard.EndpointPort = $PORT
WireGuard.PersistentKeepalive = 25
" > $CONNMANFILE
